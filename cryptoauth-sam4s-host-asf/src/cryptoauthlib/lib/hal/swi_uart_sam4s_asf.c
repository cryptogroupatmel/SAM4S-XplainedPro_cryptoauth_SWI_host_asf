/** \file swi_uart_samd21_asf.c
 * ATXMEGA's ATCA Hardware abstraction layer for SWI interface over UART drivers.
 *
 * Prerequisite: add UART Polled support to application in Atmel Studio
 *
 * Copyright (c) 2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 */

#include <stdlib.h>
#include <stdio.h>
#include "swi_uart_sam4s_asf.h"
#include "basic/atca_helpers.h"

/** \defgroup hal_ Hardware abstraction layer (hal_)
 *
 * \brief
 * These methods define the hardware abstraction layer for communicating with a CryptoAuth device
 *
   @{ */

#define USART_SERIAL                 USART1
#define USART_SERIAL_ID              ID_USART1
#define USART_SERIAL_BAUDRATE        230400
#define USART_SERIAL_CHAR_LENGTH     US_MR_CHRL_7_BIT	//US_MR_CHRL_8_BIT
#define USART_SERIAL_PARITY          US_MR_PAR_NO
#define USART_SERIAL_STOP_BIT        US_MR_NBSTOP_1_BIT

#define PINS_USART1_PIO      PIOA
#define PINS_USART1_ID       ID_USART1
#define PINS_USART1_TYPE     PIO_PERIPH_A
#define PINS_USART1_ATTR     PIO_DEFAULT
#define PINS_USART1_MASK     (PIO_PA21A_RXD1| PIO_PA22A_TXD1 )

/** \brief usart configuration struct */
static sam_usart_opt_t config_usart;

/** \brief Implementation of SWI UART init.
 * \param[in] ATCASWIMaster_t instance
 * \return ATCA_STATUS
 */
ATCA_STATUS swi_uart_init(ATCASWIMaster_t *instance)
{
	ATCA_STATUS status = ATCA_SUCCESS;
	// set the pins to use the usart peripheral
	pio_configure(PINS_USART1_PIO, PINS_USART1_TYPE, PINS_USART1_MASK, PINS_USART1_ATTR);
	// enable the usart peripherial clock1
	pmc_enable_periph_clk(ID_USART1);

	config_usart.baudrate = USART_SERIAL_BAUDRATE;
	config_usart.char_length = USART_SERIAL_CHAR_LENGTH;
	config_usart.parity_type = USART_SERIAL_PARITY;
	config_usart.stop_bits = USART_SERIAL_STOP_BIT;
	config_usart.channel_mode = US_MR_CHMODE_NORMAL;

	sysclk_enable_peripheral_clock(USART_SERIAL_ID);
		
	status = usart_init_rs232(USART_SERIAL, &config_usart, sysclk_get_peripheral_hz());
	
	usart_enable_tx(USART_SERIAL);
	usart_enable_rx(USART_SERIAL);
		
	return status;
}

/** \brief Implementation of SWI UART deinit.
 * \param[in] ATCASWIMaster_t instance
 * \return ATCA_STATUS
 */
ATCA_STATUS swi_uart_deinit(ATCASWIMaster_t *instance)
{
	ATCA_STATUS status = ATCA_SUCCESS;
	
	while ( !usart_is_tx_empty(USART_SERIAL) );	// Make sure all transmission has completed

	usart_reset(USART_SERIAL);

	return status;
}

/** \brief implementation of SWI UART change baudrate.
 * \param[in] ATCASWIMaster_t instance
 * \param[in] baudrate (typically 230400 , 160000 or 115200)
 */
void swi_uart_setbaud(ATCASWIMaster_t *instance, uint32_t baudrate)
{
	while ( !usart_is_tx_empty(USART_SERIAL) );	// Make sure all transmission has completed

	// Disable USART Tx & Rx
	usart_disable_tx(USART_SERIAL);
	usart_disable_rx(USART_SERIAL);
	
	// Set baudrate for USART module
	config_usart.baudrate = baudrate;
	usart_init_rs232(USART_SERIAL, &config_usart, sysclk_get_peripheral_hz());
	
	// Enable USART Tx & Rx
	usart_enable_tx(USART_SERIAL);
	usart_enable_rx(USART_SERIAL);
}


/** \brief implementation of SWI UART change mode.
 * \param[in] ATCASWIMaster_t instance
 * \param[in] mode (TRANSMIT_MODE or RECEIVE_MODE)
 */
void swi_uart_mode(ATCASWIMaster_t *instance, uint8_t mode)
{
	if ( mode == RECEIVE_MODE ) {
		usart_disable_tx(USART_SERIAL);
		//usart_enable_rx(USART_SERIAL);
	}
	else if ( mode == TRANSMIT_MODE ) {
		usart_enable_tx(USART_SERIAL);
		//usart_disable_rx(USART_SERIAL);
	}
}

/** \brief discover UART buses available for this hardware
 * this maintains a list of logical to physical bus mappings freeing the application
 * of the a-priori knowledge
 * \param[in] swi_uart_buses - an array of logical bus numbers
 * \param[in] max_buses - maximum number of buses the app wants to attempt to discover
 */
void swi_uart_discover_buses(int swi_uart_buses[], int max_buses)
{
	/* if every SERCOM was a likely candidate bus, then would need to initialize the entire array to all SERCOM n numbers.
	 * As an optimization and making discovery safer, make assumptions about bus-num / SERCOM map based on SAMD21 Xplained Pro board
	 * If you were using a raw SAMD21 on your own board, you would supply your own bus numbers based on your particular hardware configuration.
	 */
	swi_uart_buses[0] = 1;
}


/** \brief HAL implementation of SWI UART send byte over ASF.  This function send one byte over UART
 * \param[in] ATCASWIMaster_t instance
 * \param[in] txdata pointer to bytes to send
 * \return ATCA_STATUS
 */
ATCA_STATUS swi_uart_send_byte(ATCASWIMaster_t *instance, uint8_t data)
{
	int8_t retries = 90;
	uint32_t bitdata = (uint32_t)data;

	while ( retries > 0 ) {
		if ( usart_putchar(USART_SERIAL, bitdata) == 0 ) {
			break;
		}
		retries--;
	}

	if ( retries <= 0x00 ) {
		return ATCA_TIMEOUT;
	}
	else {
		return ATCA_SUCCESS;
	}
}

/** \brief HAL implementation of SWI UART receive bytes over ASF.  This function receive one byte over UART
 * \param[in] ATCASWIMaster_t instance
 * \param[inout] rxdata pointer to space to receive the data
 * \return ATCA_STATUS
 */
ATCA_STATUS swi_uart_receive_byte(ATCASWIMaster_t *instance, uint8_t *data)
{
	uint16_t retries = 1000000;	// This retry is used as a limitation, in case the number of bytes received are different than expected
	uint32_t bitdata = 0x00;

	while ( retries > 0 ) {
		if( usart_read(USART_SERIAL, &bitdata) == 0 ) {
			break;
		}
		retries--;
	}

	*data = (uint8_t)bitdata;

	if ( retries <= 0x00 ) {
		return ATCA_TIMEOUT;
	}
	else {
		return ATCA_SUCCESS;
	}
}

/** @} */
