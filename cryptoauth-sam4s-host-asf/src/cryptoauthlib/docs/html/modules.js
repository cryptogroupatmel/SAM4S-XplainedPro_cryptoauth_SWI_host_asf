var modules =
[
    [ "Configuration (cfg_)", "a00189.html", "a00189" ],
    [ "ATCACommand (atca_)", "a00190.html", "a00190" ],
    [ "ATCADevice (atca_)", "a00191.html", "a00191" ],
    [ "ATCAIface (atca_)", "a00192.html", "a00192" ],
    [ "Certificate manipulation methods (atcacert_)", "a00193.html", "a00193" ],
    [ "Basic Crypto API methods (atcab_)", "a00194.html", "a00194" ],
    [ "Software crypto methods (atcac_)", "a00195.html", "a00195" ],
    [ "Hardware abstraction layer (hal_)", "a00196.html", "a00196" ],
    [ "Host side crypto methods (atcah_)", "a00197.html", "a00197" ],
    [ "TLS integration with ATECC (atcatls_)", "a00198.html", "a00198" ]
];