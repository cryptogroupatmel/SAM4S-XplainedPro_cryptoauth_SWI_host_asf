var NAVTREE =
[
  [ "CryptoAuthLib", "index.html", [
    [ "CryptoAuthLib - Atmel CryptoAuthentication Library", "index.html", null ],
    [ "app directory - Purpose", "a00002.html", null ],
    [ "License", "a00004.html", null ],
    [ "basic directory - Purpose", "a00006.html", null ],
    [ "crypto directory - Purpose", "a00008.html", null ],
    [ "hal directory - Purpose", "a00010.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"a00002.html",
"a00059.html#a36f2561af7347bfca3d236e436009d30",
"a00064.html#a6e6b931cf58debb1a23b87c16c7b4d47",
"a00090.html#a22bd6643f31f1d75dc3e7ea939f468cda460a9516a4da1c7fc747208b2b9b2c77",
"a00141.html",
"a00166.html#a99670c1aa8f42045dad99468bac90a64",
"a00170.html#a163b1f9211382cc3c32002c0379e827c",
"a00183.html#a43db5788231e83e15d0f86f1024b329f",
"a00188.html#a52b6d0ed023c20279b5cf7d6de75bf87",
"a00191.html#ggafb234ccd6a80d09c0efbe336c2354267a3488f672341dda0ad20508ad888280ad",
"a00193.html#gac3f22b75f15e8508f35d99b95d955842",
"a00194.html#gab4da1bc9de16611b4c1b27ba8b00c819",
"a00196.html#ga6a2579114d13238c7ba20d147a74090a",
"a00196.html#gaf89a7a4080a47aa10a5504291db285bf",
"a00197.html#gaef11169e6cbbd8825f21b8cdcf5c1f0e",
"globals_v.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';