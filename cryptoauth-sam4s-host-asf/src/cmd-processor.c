/** \file cmd_processor.c
* simple command processor for test console
*
* Copyright (c) 2015 Atmel Corporation. All rights reserved.
*
* \asf_license_start
*
* \page License
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. The name of Atmel may not be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
* 4. This software may only be redistributed and used in connection with an
*    Atmel microcontroller product.
*
* THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
* EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
* OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
* ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* \asf_license_stop
 */ 

#include <asf.h>
#include <string.h>
#include <delay.h>
#include "cbuf.h"
#include "cryptoauthlib.h"
#include "test/atca_unit_tests.h"
#include "test/atca_basic_tests.h"
#include "test/atca_crypto_sw_tests.h"



#define USART_SERIAL                 USART1
#define USART_SERIAL_ID              ID_USART1
#define USART_SERIAL_BAUDRATE        115200
#define USART_SERIAL_CHAR_LENGTH     US_MR_CHRL_7_BIT	//US_MR_CHRL_8_BIT
#define USART_SERIAL_PARITY          US_MR_PAR_NO
#define USART_SERIAL_STOP_BIT        US_MR_NBSTOP_1_BIT

#define PINS_USART1_PIO      PIOA
#define PINS_USART1_ID       ID_USART1
#define PINS_USART1_TYPE     PIO_PERIPH_A
#define PINS_USART1_ATTR     PIO_DEFAULT
#define PINS_USART1_MASK     (PIO_PA21A_RXD1| PIO_PA22A_TXD1 )

#include "cmd-processor.h"
			
int help(void)
{
	printf("\r\nUsage:\r\n");
	printf("508  - set target device to ATECC508A\r\n");
	printf("204  - set target device to ATSHA204A\r\n");
	printf("108  - set target device to ATECC108A\r\n");
	printf("u204 - run unit tests for SHA204A\r\n");
	printf("b204 - run basic tests for SHA204A\r\n");
	printf("u108 - run unit tests for ECC108A\r\n");
	printf("u508 - run unit tests for ECC508A\r\n");
	printf("b508 - run basic tests on ECC508A\r\n");
	printf("lockstat - zone lock status\r\n");
	printf("lockcfg  - lock config zone\r\n");
	printf("lockdata - lock data and OTP zones\r\n");	
	printf("cd  - run unit tests on cert data\r\n");
	printf("cio - run unit tests on cert i/o\r\n");
	printf("info - get the chip revision\r\n");
	printf("sernum - get the chip serial number\r\n");
    printf("crypto - run unit tests for software crypto functions\r\n");
		
	printf("\r\n");
	return ATCA_SUCCESS;
}

uint8_t cmdbytes[128];


ATCA_STATUS isDeviceLocked( uint8_t zone, bool *isLocked )
{
	ATCA_STATUS status;
	
	atcab_init( gCfg );
	status = atcab_is_locked(zone, isLocked );
	atcab_release();
	return status;
}

ATCA_STATUS lockstatus(void)
{
	ATCA_STATUS status;		
	bool dataIsLocked=0xff, cfgIsLocked=0xff;
	
	if ( (status = isDeviceLocked( LOCK_ZONE_CONFIG, &cfgIsLocked )) != ATCA_SUCCESS )
		printf("can't read cfg lock\r\n");
			
	if ( (status = isDeviceLocked( LOCK_ZONE_DATA, &dataIsLocked )) != ATCA_SUCCESS )
		printf("can't read data lock\r\n");

	printf("Config Zone Lock: %s\r\n", cfgIsLocked == 0x01 ? "locked" : "unlocked");
	printf("Data Zone Lock  : %s\r\n", dataIsLocked == 0x01 ? "locked" :"unlocked" );
	
	return status;
}

uint8_t lock_config_zone(void)
{
	ATCA_STATUS status;
	uint8_t lock_response;
	
	atcab_init( gCfg );
	
	if ( (status = atcab_lock_config_zone( &lock_response )) != ATCA_SUCCESS )
		printf("\r\nunable to lock config zone\r\n");

	atcab_release();
	return lock_response;		
}

uint8_t lock_data_zone(void)
{
	ATCA_STATUS status;
	uint8_t lock_response;
		
	atcab_init( gCfg );
	if ( (status = atcab_lock_data_zone( &lock_response )) != ATCA_SUCCESS )
		printf("\r\nunable to lock data zone\r\n");
	
	atcab_release();
	return lock_response;		
}

ATCA_STATUS getinfo( uint8_t *revision )
{
	ATCA_STATUS status;
	
	atcab_init( gCfg ); // contain SWI initialization
	status =  atcab_info( revision );
	atcab_release();	

	return status;
}

ATCA_STATUS getsernum( uint8_t *sernum )
{
	ATCA_STATUS status;
	
	atcab_init( gCfg );
	status = atcab_read_serial_number( sernum );
	atcab_release();
	
	return status;
}

int parseCmd( char *commands )
{
	char *cmds = NULL;
	ATCA_STATUS status;
	
	if ( (cmds = strstr( commands, "help")) ) {
		help();
	}
	else if ( (cmds = strstr( commands, "u508")) ) {
		atca_unit_tests(ATECC508A);
	}
	else if ( (cmds = strstr( commands, "b508")) ) {
		atca_basic_tests(ATECC508A);
	}
	else if ( (cmds = strstr( commands, "u108")) ) {
		atca_unit_tests(ATECC108A);
	}
	else if ( (cmds = strstr( commands, "b108")) ) {
		atca_basic_tests(ATECC108A);
	}
	else if ( (cmds = strstr( commands, "u204")) ) {
		atca_unit_tests(ATSHA204A);
	}
	else if ( (cmds = strstr( commands,"b204")) ) {
		atca_basic_tests(ATSHA204A);
	}
	else if ( (cmds = strstr( commands, "cd")) ) {
		certdata_unit_tests();
	}
	else if ( (cmds = strstr( commands, "cio")) ) {
		certio_unit_tests();
	}
	else if ( (cmds = strstr( commands, "lockstat")) ) {
		lockstatus();
	}
	else if ( (cmds = strstr( commands, "lockcfg")) ) {
		if ( lock_config_zone() != ATCA_SUCCESS )
			printf("Could not lock config zone\r\n");
		lockstatus();
	}
	else if ( (cmds = strstr( commands, "lockdata")) ) {
		if ( lock_data_zone() != ATCA_SUCCESS )
			printf("Could not lock data zone\r\n");
		lockstatus();
	}
	else if ( (cmds = strstr( commands, "508") ) ) {
		gCfg = &cfg_ateccx08a_swi_default;
		gCfg->devtype = ATECC508A;
		gCfg->atcaswi.bus = 1;
		printf("\r\ncurrent device: ATECC508A\r\n");
	}
	else if ( (cmds = strstr( commands, "204") ) ) {
		gCfg = &cfg_sha204a_swi_default;
		printf("\r\ncurrent device: ATSHA204A\r\n");
	}
	else if ( (cmds = strstr( commands, "108") ) ) {
		gCfg = &cfg_ateccx08a_swi_default;
		gCfg->devtype = ATECC108A;	
		printf("\r\ncurrent device: ATECC108A\r\n");	 	
	}
	else if ( (cmds = strstr( commands, "info")) ) {
		uint8_t revision[4];
		char displaystr[15];
		int displaylen = sizeof(displaystr);
		
		gCfg = &cfg_ateccx08a_swi_default;
		gCfg->atcaswi.bus = 1;
		status = getinfo(revision);
		if ( status == ATCA_SUCCESS ) {
			// dump revision
			atcab_bin2hex(revision, 4, displaystr, &displaylen );
			printf("\r\nx08 revision:\r\n%s\r\n", displaystr);
		}
	}
	else if ( (cmds = strstr( commands, "sernum")) ) {
		uint8_t serialnum[ATCA_SERIAL_NUM_SIZE];
		char displaystr[30];
		int displaylen = sizeof(displaystr);
		
		gCfg = &cfg_ateccx08a_swi_default;
		gCfg->atcaswi.bus = 1;
		status = getsernum(serialnum);
		if ( status == ATCA_SUCCESS ) {
			// dump serial num
			atcab_bin2hex(serialnum, ATCA_SERIAL_NUM_SIZE, displaystr, &displaylen );
			printf("\r\nx08 serial number:\r\n%s\r\n", displaystr);
		}
	}
	else if ( (cmds = strstr( commands, "crypto")) ) {
		atca_crypto_sw_tests();
	}
	else if ( (cmds = strstr( commands, "test")) ) {
		ATCA_STATUS status = ATCA_SUCCESS;
		static const uint8_t public_key_ref[64] = {
			0x8F, 0x8D, 0x18, 0x2B, 0xD8, 0x19, 0x04, 0x85, 0x82, 0xA9, 0x92, 0x7E, 0xA0, 0xC5, 0x6D, 0xEF,
			0xB4, 0x15, 0x95, 0x48, 0xE1, 0x1C, 0xA5, 0xF7, 0xAB, 0xAC, 0x45, 0xBB, 0xCE, 0x76, 0x81, 0x5B,
			0xE5, 0xC6, 0x4F, 0xCD, 0x2F, 0xD1, 0x26, 0x98, 0x54, 0x4D, 0xE0, 0x37, 0x95, 0x17, 0x26, 0x66,
			0x60, 0x73, 0x04, 0x61, 0x19, 0xAD, 0x5E, 0x11, 0xA9, 0x0A, 0xA4, 0x97, 0x73, 0xAE, 0xAC, 0x86
		};
		uint8_t public_key[64];

		gCfg = &cfg_ateccx08a_swi_default;
		gCfg->atcaswi.bus = 1;

		status = atcab_init(gCfg);
		printf("atcab_init(), status: %.2x\r\n", status);

		status = atcab_get_pubkey(0, public_key);
		printf("atcab_get_pubkey(), status: %.2x\r\n", status);

		//TEST_ASSERT_EQUAL_MEMORY(public_key_ref, public_key, sizeof(public_key_ref));

		status = atcab_release();
		printf("atcab_release(), status: %.2x\r\n", status);		 
	}
	else if ( strlen(commands) ) {
		printf("\r\nsyntax error in command: %s\r\n", commands);
	}
	
	return ATCA_SUCCESS;
}


int processCmd(void)
{
	static char cmd[256];
	uint16_t i = 0;
	while( !CBUF_IsEmpty(cmdQ) && i < sizeof(cmd))
		cmd[i++] = CBUF_Pop( cmdQ );
	cmd[i] = '\0';
	//printf("\r\n%s\r\n", command );
	parseCmd(cmd);
	printf("$ ");
	
	return ATCA_SUCCESS;
}
