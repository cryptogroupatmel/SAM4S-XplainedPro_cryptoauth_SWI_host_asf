/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * This is a bare minimum user application template.
 *
 * For documentation of the board, go \ref group_common_boards "here" for a link
 * to the board-specific documentation.
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to board_init()
 * -# Basic usage of on-board LED and button
 * -# "Insert application code here" comment
 *
 */

#include <asf.h>
#include "cbuf.h"
#include "cryptoauthlib.h"
#include "cmd-processor.h"

int main(void)
{
	sysclk_init();
	board_init();

	// Initialize interrupt vector table support.
	irq_initialize_vectors();

	// Enable interrupts
	cpu_irq_enable();

	stdio_usb_init();

	uint8_t ch;

	while (true) {
		ch = 0;
		if (udi_cdc_is_rx_ready())
		scanf("%c",&ch);
		
		if (ch) {
			printf("%c",ch); // echo to output
			if ( ch == 0x0d || ch == 0x0a ) {
				processCmd();
				} else {
				CBUF_Push( cmdQ, ch );  // queue character into circular buffer
			}
		}
	}
}

