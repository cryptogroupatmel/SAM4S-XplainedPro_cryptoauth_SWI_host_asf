Cryptoauthlib Host Example using SAM4S Xplained Pro
=====================================================

Project: cryptoauth-sam4s-host-asf.atsln

This example demonstrates the basic-test of the CryptoAuthLib library for Atmel ECC508A device 
that communicate using SWI (Single Wire Interface) protocol with SAM4S Xplained Pro board. 
All drivers for this example are created by Atmel Software Framework (ASF).


Prerequisites for this example
--------------------------------
Software:
  - Atmel Studio 6.2 or
  - Atmel Studio 7
  
Hardware:
  - Atmel SAM4S Xplained Pro
  - Atmel ATCK101 top board with socket


Building the Example Source Code
----------------------------------
If you are using Atmel Studio 6.2, then you should load the project file:
  - cryptoauth-d21-swi-asf-host_6_2.atsln
If you are using Atmel Studio 7, then you should load the project file:
  - cryptoauth-d21-swi-asf-host.atsln

Once the project has been loaded, you can build it with "Rebuild Solution" under the Build
menu.  Flash the SAM4S Xplained Pro development board using the Debugging button in the Debug toolbar
or press F5 to instant access.


Using the Example Project
--------------------------
There are two USB ports on the SAM4S Xplained Pro.  One is labeled "EDBG USB" and is
used to flash the code into the MCU with Atmel Studio.  The second USB port is labeled
"Target USB", a CDC USB port and is used for the console interface to the example.

Connect your host computer to the EDBG USB to program it.  

Connect your host computer to the Target USB CDC port in order to see a console interface 
you can use to exercise the example after it has been programmed.

Use a terminal program on your host and connect it to the virtual comm port of the 
SAM4S Xplained Pro which should be created when you plg the Target USB CDC port into your
PC, Linux, or OS X machine.  This particular step will vary on each computer and operating system.

The communication parameters are:
  - 115,200 baud
  - 8 bit word
  - No parity
  - 1 stop bit
 
Once you've connected to the terminal serial usb you can type 'help' and see
all the application command list.


Progress Log:
--------------
20170116: all cryptoautlib basic and unit test have been successfully executed on SAM4S Xplained Pro using new generated SAM4S HAL SWI. There are some adjustments 
in the CryptoAuthLib library to successfully perform this example such as:

  (a) In atca_basic_tests.c some arrangement need to be made on
      void atca_basic_tests(ATCADeviceType deviceType) function,
      following code need to be added on line 214 
      gCfg->atcaswi.bus = 1;
      It is needed to adjust the number of bus of SAM4S Xplained Pro board SWI bus

  (b) In atca_unit_tests.c some arrangement need to be made on
      void atca_unit_tests(ATCADeviceType deviceType) function,
      following code need to be added on line 85 
      gCfg->atcaswi.bus = 1;
      It is also needed to adjust the number of bus of SAM4S Xplained Pro board SWI bus

  (c) In atca_command.c an arrangement need to be made to adjust timing on SAM4S
      Xplained Pro board communication with ECC508A device, 
      on line 567, the value of 1 need to be changed to 2 ms for the execution time
      of reading memory from ECC508A device (32 bytes from the device). 
      The execution time need to be added because the device need 1.1 ms, and from SHA204A device
      execution time for read command can reach 4 ms.